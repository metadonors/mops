mops
====

Meta Operations CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/mops.svg)](https://npmjs.org/package/mops)
[![Downloads/week](https://img.shields.io/npm/dw/mops.svg)](https://npmjs.org/package/mops)
[![License](https://img.shields.io/npm/l/mops.svg)](https://github.com/Projects/mops/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g meta-ops-cli
$ mops COMMAND
running command...
$ mops (-v|--version|version)
meta-ops-cli/0.0.1 linux-x64 node-v10.0.0
$ mops --help [COMMAND]
USAGE
  $ mops COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`mops finish`](#mops-finish)
* [`mops help [COMMAND]`](#mops-help-command)
* [`mops init`](#mops-init)
* [`mops upgrade`](#mops-upgrade)

## `mops finish`

Finish upgrade

```
USAGE
  $ mops finish

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/finish.js](https://github.com/Projects/mops/blob/v0.0.1/src/commands/finish.js)_

## `mops help [COMMAND]`

display help for mops

```
USAGE
  $ mops help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.6/src/commands/help.ts)_

## `mops init`

Insert to your basic rancher configuration

```
USAGE
  $ mops init

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/init.js](https://github.com/Projects/mops/blob/v0.0.1/src/commands/init.js)_

## `mops upgrade`

Upgrade stacks

```
USAGE
  $ mops upgrade

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/upgrade.js](https://github.com/Projects/mops/blob/v0.0.1/src/commands/upgrade.js)_
<!-- commandsstop -->
