const {AutoComplete} = require('enquirer')
const backend = require('../backend')
const _ = require('lodash')

const selectEnvironment = async () => {
  try {
    const response = await backend.getEnvironments()
    const envs = _.keyBy(response.data, 'id')
    const prompt = new AutoComplete({
      name: 'env',
      message: 'Choose environment',
      choices: response.data.map(e => ({
        message: e.name,
        value: e.id,
      })),
    })
    const selected = await prompt.run()
    return envs[selected]
  } catch (error) {
    throw new Error(error)
  }
}

const selectServices = async environmentId => {
  try {
    const response = await backend.getServices(environmentId)
    const prompt = new AutoComplete({
      name: 'services',
      message: 'Choose services',
      multiple: true,
      choices: response.data.map(e => ({
        message: e.name,
        value: e.id,
      })),
    })
    const selected = await prompt.run()
    return response.data.filter(s => selected.indexOf(s.id) >= 0)
  } catch (error) {
    throw new Error(error)
  }
}

const selectStacks = async environmentId => {
  try {
    const response = await backend.getStacks(environmentId)
    const prompt = new AutoComplete({
      name: 'stacks',
      message: 'Choose stacks',
      multiple: true,
      choices: response.data.map(e => ({
        message: `${e.name}`,
        value: e.id,
      })),
    })
    const selected = await prompt.run()
    return response.data.filter(s => selected.indexOf(s.id) >= 0)
  } catch (error) {
    throw new Error(error)
  }
}

module.exports = {
  selectEnvironment,
  selectServices,
  selectStacks,
}
