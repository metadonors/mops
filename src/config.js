const Conf = require('conf')

const config = new Conf({
  projectName: 'mops',
  projectSuffix: 'cli',
})

class AppConfig {
  getBasicConfig() {
    return config.get('basic')
  }

  setBasicConfig(info) {
    return config.set('basic', info)
  }
}

module.exports = new AppConfig()
