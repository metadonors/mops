const {Command} = require('@oclif/command')
const {prompt} = require('enquirer')

const config = require('../config')

class InitCommand extends Command {
  async run() {
    try {
      const data = await prompt([
        {
          type: 'input',
          name: 'url',
          message: 'Rancher URL',
        },
        {
          type: 'input',
          name: 'accessKey',
          message: 'Rancher API KEY',
        },
        {
          type: 'input',
          name: 'accessSecret',
          message: 'Rancher SECRET KEY',
        },
      ])
      config.setBasicConfig(data)
    } catch (error) {
      this.error(error)
    }
  }
}

InitCommand.description = `Insert to your basic rancher configuration
...
Extra documentation goes here
`

module.exports = InitCommand
