const {Command, flags} = require('@oclif/command')
const {selectEnvironment, selectStacks} = require('../ui/common')
const backend = require('../backend')

class UpgradeCommand extends Command {
  async run() {
    const env = await selectEnvironment()
    const stacks = await selectStacks(env.id)

    stacks.map(async stack => {
      const services = await backend.getStackServices(env.id, stack.id)
      services.data.map(service => {
        if (service.state === 'upgraded') {
          this.log(`${service.name} already upgraded (call 'finish').`)
        } else if (service.state === 'active') {
          backend.upgradeService(env.id, service.id, service.upgrade)
          this.log(`${service.name} start upgrade.`)
        } else {
          this.log(`${service.name} upgrade in progress.`)
        }
        return null
      })
    })
  }
}

UpgradeCommand.description = `Upgrade stacks
...
Extra documentation goes here
`

UpgradeCommand.flags = {
  name: flags.string({char: 'n', description: 'name to print'}),
}

module.exports = UpgradeCommand
