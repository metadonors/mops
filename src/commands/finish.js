const {Command, flags} = require('@oclif/command')
const {selectEnvironment, selectStacks} = require('../ui/common')
const backend = require('../backend')

class FinishUpgradeCommand extends Command {
  async run() {
    const env = await selectEnvironment()
    const stacks = await selectStacks(env.id)
    stacks.map(async stack => {
      const services = await backend.getStackServices(env.id, stack.id)
      services.data.map(service => {
        if (service.state === 'upgraded') {
          backend.finishUpgradeService(env.id, service.id)
          this.log(`${service.name} completing upgrade.`)
        } else if (service.state === 'active') {
          this.log(`${service.name} nothing to do.`)
        } else {
          this.log(`${service.name} upgrade still in progress.`)
        }
        return null
      })
    })
  }
}

FinishUpgradeCommand.description = `Finish upgrade
...
Extra documentation goes here
`

FinishUpgradeCommand.flags = {
  name: flags.string({char: 'n', description: 'name to print'}),
}

module.exports = FinishUpgradeCommand
