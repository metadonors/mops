const config = require('./config')
const Rancher = require('rancher-node')

const createClient = () => {
  const conf = config.getBasicConfig()
  const rancherConfig = {
    host: conf.url,
    accessKey: conf.accessKey,
    secretKey: conf.accessSecret,
  }
  return new Rancher.Client(rancherConfig)
}

module.exports = createClient()
