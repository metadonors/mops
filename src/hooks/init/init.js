const config = require('../../config')

module.exports = async function (opts) {
  if (opts.id !== 'init' && !config.getBasicConfig()) {
    this.error('Not authenticate, first call "init" command.')
    this.error(opts)
    this.exit()
  }
}
